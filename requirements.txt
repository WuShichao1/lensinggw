numpy>=1.18.1
scipy>=1.4.1
astropy>=4.0
matplotlib>=3.1.3 
lalsuite>=6.70
-e git://github.com/gipagano/lenstronomy.git@master#egg=lenstronomy
mpmath